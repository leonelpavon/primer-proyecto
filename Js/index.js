$(function () {
   $('[data-toggle="tooltip"]').tooltip();
   $('[data-toggle="popover"]').popover();
   $(".carousel").carousel({
 interval: 2000
 });
 
 $('#cliente').on('show.bs.modal', function (e) {
   console.log('el modal cliente se está mostrando');

     $('#clienteBtn, #clienteBtn2, #clienteBtn3, #clienteBtn4').removeClass("btn-warning"); 
     $('#clienteBtn, #clienteBtn2, #clienteBtn3, #clienteBtn4').addClass("btn-danger");
     $('#clienteBtn, #clienteBtn2, #clienteBtn3, #clienteBtn4').prop("disabled", true); 

 });

 $('#cliente').on('shown.bs.modal', function (e) {
   console.log('el modal cliente se mostró');
 });
 $('#cliente').on('hide.bs.modal', function (e) {
   console.log('el modal cliente se oculta');
 });
 $('#cliente').on('hidden.bs.modal', function (e) {
   console.log('el modal cliente se ocultó');
   $('#clienteBtn, #clienteBtn2, #clienteBtn3, #clienteBtn4').prop("disabled", false); 
   $('#clienteBtn, #clienteBtn2, #clienteBtn3, #clienteBtn4').removeClass("btn-danger");
   $('#clienteBtn, #clienteBtn2, #clienteBtn3, #clienteBtn4').addClass("btn-warning");
 });

});
